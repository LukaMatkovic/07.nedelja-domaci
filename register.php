<?php


$name = $email = $phone = $date = $dateofbirth = $gender = $comment = "";
$nameErr = $emailErr = $phoneErr = $dateofbirthErr = $genderErr = "";

if ($_SERVER ["REQUEST_METHOD"] == "POST") {

    if (empty($_POST["name"])) {
        $nameErr = "Name is required!";
        header('Location:index.php?name=0');
        exit;
    }
    else {
        $name=test_input($_POST["name"]);
        if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
            $nameErr = "Only letters and white space allowed!"; 
        }
    }
    if (empty($_POST["email"])) {
        header('Location:index.php?email=0');
        $emailErr = "E-mail is required!";
        exit;

    }
    else {
        $email=test_input($_POST["email"]);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format"; 
        }
    }
    if (empty($_POST["phone"])) {
        $phoneErr = "Phone is required!";
        header('Location:index.php?phone=0');
        exit;
    }
    else {
        $phone=test_input($_POST["phone"]);
    }   
        
    if (empty($_POST["dateofbirth"])) {
        $dateofbirthErr = "Date of birth is required!";
        header('Location:index.php?dateofbirth=0');
        exit;
    }
    else {
        $dateofbirth=test_input($_POST["dateofbirth"]);
        $date=date_create(test_input($_POST["dateofbirth"]));
        $date=date_format($date,"l, F j Y");    
    }

    if (empty($_POST["gender"])) {
        header('Location:index.php?gender=0');
        $genderErr = "Gender is required!";
        exit;
    }
    else {
        $gender=test_input($_POST["gender"]);
    }

    $comment=test_input($_POST["comment"]);
}

function test_input($data) {
    $data=trim($data);
    $data=stripslashes($data);
    $data=htmlspecialchars($data);
    return $data;
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="PHP, form">
        <meta name="author" content="Luka Matkovic">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,700" rel="stylesheet">
        <title>Uspesno popunjena prijava</title>
    </head>
    <body>
        <div class="rez">
        Welcome <?php echo $_GET['name'];?><br>
        Your email address is: <?php echo $_GET['email'];?><br>
        Your date of birth is: <?php echo $_GET['dateofbirth'];?><br>
        Your telephone number is: <?php echo $_GET['phone'];?><br>
        Additional information: <?php echo $_GET['comment'];?><br>
        </div>
    </body>
</html>
