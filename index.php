<?php

$name = $email = $phone = $dateofbirth = $gender = $comment = "";
$nameErr = $emailErr = $phoneErr = $dateofbirthErr = $genderErr = "";

if ($_SERVER ["REQUEST_METHOD"] == "POST") {

    if (empty($_POST["name"])) {
        $nameErr = "Name is required!";
    }
    else {
        $name=test_input($_POST["name"]);
        if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
            $nameErr = "Only letters and white space allowed!"; 
        }
    }
    if (empty($_POST["email"])) {
        $emailErr = "E-mail is required!";

    }
    else {
        $email=test_input($_POST["email"]);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format"; 
        }
    }
    if (empty($_POST["phone"])) {
        $phoneErr = "Phone is required!";
    }
    else {
        $phone=test_input($_POST["phone"]);
    }   
        
    if (empty($_POST["dateofbirth"])) {
        $dateofbirthErr = "Date of birth is required!";
    }
    else {
        $dateofbirth=test_input($_POST["dateofbirth"]);
        $date=date_create(test_input($_POST["dateofbirth"]));
        $date=date_format($date,"l, F j Y");    
    }

    if (empty($_POST["gender"])) {
        $genderErr = "Gender is required!";
    }
    else {
        $gender=test_input($_POST["gender"]);
    }

    $comment=test_input($_POST["comment"]);
    
    header("Location: register.php?name=$name&email=$email&phone=$phone&dateofbirth=$date&gender=$gender&comment=$comment");

}



function test_input($data) {
    $data=trim($data);
    $data=stripslashes($data);
    $data=htmlspecialchars($data);
    return $data;
}
?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="PHP, form">
    <meta name="author" content="Luka Matkovic">

    <title>Forme</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,700" rel="stylesheet">

</head>
<body>
    <section class="forma">
    <form action="index.php" method="post">  
        <fieldset>
            <legend>Form</legend>

            <label for="name">Name <span class="error"> *     <?php if (isset($nameErr)) { echo $nameErr; } ?><span></label><br>
            <input type="text" name="name" value="<?php if (isset($_POST["name"])) {echo $_POST["name"];}?>">

            <label for="email">E-mail <span class="error"> * <?php if (isset($emailErr)) { echo $emailErr; } ?></span></label><br>
            <input type="email" name="email" placeholder="mail@mail.com" value="<?php if (isset($_POST["email"])) {echo $_POST["email"];}?>"><br>

            <label for="phone">Telephone Number <span class="error"> * <?php if (isset($phoneErr)) { echo $phoneErr; } ?></span></label><br>
            <input type="number" name="phone" value="<?php if (isset($_POST["phone"])) {echo $_POST["phone"];}?>"><br>

            <label for="dateofbirth">Date of Birth <span class="error"> * <?php if (isset($dateofbirthErr)) { echo $dateofbirthErr; } ?></span></label><br>
            <input type="date" name="dateofbirth" value="<?php if (isset($_POST["dateofbirth"])) {echo $_POST["dateofbirth"];}?>"><br>

            <label for="gender">Gender <span class="error"> * <?php if (isset($genderErr)) { echo $genderErr; } ?></span><br></label>
            <input type="radio" name="gender" <?php if (isset($gender) && $gender=="male") echo "checked";?> value="male">Male<br>
            <input type="radio" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?> value="female">Female<br>
            <input type="radio" name="gender" <?php if (isset($gender) && $gender=="other") echo "checked";?> value="other">Other<br>

            <label for="comment">Write Your Comments</label><br>
            <textarea name="comment" rows="10" cols="50"><?php echo $comment;?></textarea><br>

        </fieldset>
        <input type="submit" value="Submit" class="save">
    </form
    </section>
  
</body>
</html>